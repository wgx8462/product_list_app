import 'package:flutter/material.dart';
import 'package:product_list_app/model/product_item.dart';

class ComponentProductItem extends StatelessWidget {
  const ComponentProductItem({
    super.key,
    required this.productItem,
    required this.callback
  });

  final ProductItem productItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Image.asset(
              'assets/${productItem.imageName}',
            width: 300,
            height: 300,
            fit: BoxFit.contain,
          ),
          Text(
              productItem.name,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                  '${productItem.price}',
                style: TextStyle(
                  color: Colors.redAccent,
                ),
              ),
              const Text(
                  ' 원',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
