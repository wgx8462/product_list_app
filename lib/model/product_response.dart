class ProductResponse {
  num id;
  String name;
  num price;
  String imageName;
  String dateCreate;

  ProductResponse(this.id, this.name, this.price, this.imageName, this.dateCreate);

  factory ProductResponse.formJson(Map<String, dynamic> json) {
    return ProductResponse(
      json['id'],
      json['name'],
      json['price'],
      json['imageName'],
      json['dateCreate'],
    );
  }
}