import 'package:product_list_app/model/product_item.dart';

class ProductListResult {
  String msg;
  num code;
  List<ProductItem> list;
  num totalCount;

  ProductListResult(this.msg, this.code, this.list, this.totalCount);

  factory ProductListResult.fromJson(Map<String, dynamic> json) {
    return ProductListResult(
      json['msg'],
      json['code'],
      json['list'] != null
          ? (json['list'] as List).map((e) => ProductItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
    );

    // json['lsit'] != null 은 두 개가 다르면 참이라는 뜻이다. json['lsit']는 null값이 아닐 경우에 참인것이다.
    // ? a : b 는 삼항연산자인다. a부분이 참일경우 나오는 코드이고 b부분은 거짓일 경우 나오는 부분이다.

    //json으로 [{name:'asdfs'},{name:'dasfd'}]이런 String을 받아서 Object의 List로 바꾸고 난후에
    // 그럼 어쨋든 List니까 한 뭉탱이씩 던져줄 수 있다.
    // 근데.. 한뭉탱이씩 던지겠다 하면서 한 뭉텡이 부르는거에 이름이 바로 e
    // 다 작은그릇으로 바꿔치기 한다음에
    // 다시 그것들을 싹 가져와서 리스트로 촥 하고 정리해줌.
  }
}
