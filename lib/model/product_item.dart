class ProductItem {
  num id;
  String name;
  String imageName;
  num price;

  ProductItem(this.id, this.name, this.imageName, this.price);

  factory ProductItem.fromJson(Map<String, dynamic> json) {
    return ProductItem(
      json['id'],
      json['name'],
      json['imageName'],
      json['price'],
    );
  }
}
