import 'package:dio/dio.dart';
import 'package:product_list_app/config/config_api.dart';
import 'package:product_list_app/model/product_detail_result.dart';
import 'package:product_list_app/model/product_list_result.dart';

class RepoProduct {
  /**
   * 복수R
   */
  Future<ProductListResult> getProducts() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/product/all'; // 엔드포인트

    final response = await dio.get(_baseUrl,
        options: Options(
            followRedirects: false, // 400번 500번 오류는 여기서 걸러진다.
            validateStatus: (status) {
              return status == 200;
            }));

    return ProductListResult.fromJson(response.data);
  }

  Future<ProductDetailResult> getProdust(num id) async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/product/detail/{id}';

    final response = await dio.get(_baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return ProductDetailResult.fromJson(response.data);
  }
}
