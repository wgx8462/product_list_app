import 'package:flutter/material.dart';
import 'package:product_list_app/components/component_product_item.dart';
import 'package:product_list_app/model/product_item.dart';
import 'package:product_list_app/pages/page_product_detail.dart';
import 'package:product_list_app/repository/repo_product.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  List<ProductItem> _list = [];

  // Future<void> 미래에 무언가 올거야 근데 넌 신경쓰지마
  // 이 메서드가 repo 호출해서 데이터 받아온 다음에..
  // setstate해서 _list 교체 할거다.
  Future<void> _loadList() async {
    await RepoProduct()
        .getProducts()
        .then((res) => {
              setState(() {
                _list = res.list;
              })
            })
        .catchError((err) => {debugPrint(err)});
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '상품 리스트',
        ),
        centerTitle: true,
      ),
      drawer: Drawer(),
      body: GridView.builder(
        shrinkWrap: true,
          itemCount: _list.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 60 / 100,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
          ),
          itemBuilder: (BuildContext context, int idx) {
          return ComponentProductItem(productItem: _list[idx], callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageProductDetail(id: _list[idx].id)));
          });
          }
      ),
    );
  }
}
