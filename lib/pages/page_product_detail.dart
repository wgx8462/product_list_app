import 'package:flutter/material.dart';
import 'package:product_list_app/model/product_response.dart';
import 'package:product_list_app/repository/repo_product.dart';

class PageProductDetail extends StatefulWidget {
  const PageProductDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageProductDetail> createState() => _PageProductResponseState();
}

class _PageProductResponseState extends State<PageProductDetail> {
  ProductResponse? _detail;

  @override
  void initState() {
    setState(() {
      super.initState();
      _loadDetail();
    });
  }

  Future<void> _loadDetail() async {
    await RepoProduct().getProdust(widget.id)
        .then((res) =>
    {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상세보기'),
      ),
      body: buildBody(context),


    );
  }

  Widget buildBody(BuildContext context) {
    if (_detail == null) { // 스켈레톤 ui
      return Text('데이터 로딩중..');
    } else {
      return Center(
        child: Column(
          children: [
            Text('${widget.id}'),
            Image.asset(
                'assets/${_detail!.imageName}',
              width: 300,
              height: 300,
              fit: BoxFit.contain,
            ),
            Text(_detail!.name),
            Text('${_detail!.price}원'),
            Text(_detail!.dateCreate),
          ],
        ),
      );
    }
  }
}